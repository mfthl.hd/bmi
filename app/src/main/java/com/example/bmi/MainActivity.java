package com.example.bmi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText Tinggi, Berat;
    TextView Hasil;
    Button btnCalculate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Tinggi = (EditText) findViewById(R.id.tinggi);
        Berat = (EditText) findViewById(R.id.berat);
        Hasil = (TextView) findViewById(R.id.hasil);
        btnCalculate = (Button) findViewById(R.id.hitung);

        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String status;
                float tb = Float.parseFloat(Tinggi.getText().toString());
                float tb_m = tb/100;
                float bb = Float.parseFloat(Berat.getText().toString());
                double fix_tb = Math.pow(tb_m,2);
                double bmi = bb/fix_tb;
                if (bmi < 18.5) {
                    status = "BB Kurang";
                } else if (bmi >= 18.5 && bmi < 23) {
                    status = "BB Normal";
                } else if (bmi >= 23 && bmi < 30) {
                    status = "BB Berlebih";
                } else {
                    status = "Obesitas";
                }
                Hasil.setText(status);
            }
        });
    }
}